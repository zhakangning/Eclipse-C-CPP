#ifndef OPENCVTEST_H
#define OPENCVTEST_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "opencv2/opencv.hpp"
#include <iostream>

using namespace std;
using namespace cv;

static string xmlPath="haarcascade_frontalface_default.xml";
static string videoPath = "";
static string filePath = "pic2";

//xmlpath 字符串记录那个.xml文件的路径
string hashValue(Mat &src);      //得到图片的哈希值
int HanmingDistance(string &str1,string &str2);       //求两张图片的汉明距离
void detectAndDisplay(Mat image, Mat &face);

//调用现成的矩阵计算函数进行图像旋转
void rotateImgDemo();

//图像基本操作
void funcOfMatTest();
void imgReadDisplaySave();

/*
 * 图像旋转算法描述
 * 1、计算中心点坐标
 * 2、计算每个像素点于中心点的相对坐标
 * 3、计算旋转后每个像素点的相对坐标
 * 4、把每个像素点的相对坐标换算为绝对坐标，即矩阵的行和列
 * 5、把每个像素点移动到旋转后的绝对坐标上
 */
Mat myRotateImg(Mat &srcImage, int angle);

//镜像操作
void remapTest();

//平移操作，图片大小不变
Mat imageTranslation1(Mat & srcImage, int xoffset, int yoffset);
//平移操作，图片大小改变
Mat imageTranslation2(Mat & srcImage, int xOffset, int yOffset);
void imgTranslationTest();
//基于等间隔提取图像缩放
void imageReduction1();
//计算区域平均值
Vec3b areaAverage(const Mat &srcImage, Point_<int> leftPoint, Point_<int> rightPoint);
void imageReduction2();
Mat angleRotate(Mat &src, int angle);
void imgRotateTest();
//仿射变换测试
void affineTranslation();
//videoCapture使用示例
void videoCaptureTest();
void useCamera();
void writeVideo();

//视频质量评价
//计算PSNR，返回值30~50dB，值越大越好
double PSNR(const Mat & I1, const Mat & I2);
//计算MSSIM结构相似性，返回值从0到1，值越大越好
Scalar MSSIM(const Mat & i1, const Mat & i2);

//图像基础应用操作
//利用createTrackbar进行二值化
void trackbarTest();
//createTrackbar的回调响应函数
void onChangeTrackBar(int pos, void *data);

//区域提取
//Rect选择感兴趣的区域
static Mat srcImage;
//方法1：利用Rect选择区域(100, 180, 150, 50)
void regionExtraction(int xRoi, int yRoi, int widthRoi, int heightRoi);
void regionExtractionTest();

//鼠标按键获取感兴趣的区域1
static Rect roiRect;
static Point startPoint;
static Point endPoint;
static bool downFlag = false;//完成所选区域标志位
static bool upFlag = false;
void mouseEvent(int event, int x, int y, int flags, void *data);
void mouseEventTest();

#endif // OPENCVTEST_H
