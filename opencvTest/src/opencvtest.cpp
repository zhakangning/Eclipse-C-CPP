#include "opencvtest.h"

string hashValue(Mat &src)      //得到图片的哈希值
{
	imshow("1",src);
	cvWaitKey(0);
	string rst(64,'\0');
	Mat img;
	cout<<"channels = "<<src.channels()<<endl;
	if(src.channels() == 3)
		cvtColor(src, img, CV_BGR2GRAY);
	else
		img = src.clone();

	imshow("1",img);
	cvWaitKey(0);
	resize(img, img, Size(8,8));
	imshow("1",img);
	cvWaitKey(0);
	uchar *pData;
	for(int i=0;i<img.rows;i++)
	{
		pData=img.ptr<uchar>(i);
		for(int j=0;j<img.cols;j++)
		{
			pData[j]=pData[j]/4;
		}
	}

	int average = mean(img).val[0];
	Mat mask=(img>=(uchar)average);
	int index=0;
	for(int i=0;i<mask.rows;i++)
	{
		pData = mask.ptr<uchar>(i);
		for(int j=0;j<mask.cols;j++)
		{
			if(pData[j]==0)
				rst[index++]='0';
			else
				rst[index++]='1';
		}
	}
	return rst;
}

int HanmingDistance(string &str1,string &str2)       //求两张图片的汉明距离
{
	if((str1.size()!=64)||(str2.size()!=64))
		return -1;
	int diff=0;
	for(int i=0;i<64;i++)
	{
		if(str1[i]!=str2[i])
			diff++;
	}
	return diff;
}

void detectAndDisplay(Mat image, Mat &face)
{
	CascadeClassifier ccf;      //创建脸部对象
	ccf.load(xmlPath);           //导入opencv自带检测的文件
	vector<Rect> faces;
	Mat gray;

	cvtColor(image, gray, CV_BGR2GRAY);
	//	imshow("1",image);
	//	cvWaitKey(0);
	equalizeHist(gray, gray);
	//	imshow("1",image);
	//	cvWaitKey(0);
	ccf.detectMultiScale(gray, faces, 1.1, 3, 0, Size(50,50), Size(500,500));
	for(vector<Rect>::const_iterator iter=faces.begin();iter!=faces.end();iter++)
	{
		rectangle(image, *iter, Scalar(0, 0, 255), 2, 8); //画出脸部矩形
	}
	//	imshow("1",image);
	//		cvWaitKey(0);
	for(size_t i=0;i<faces.size();i++)
	{
		Point center(faces[i].x + faces[i].width / 2, faces[i].y + faces[i].height / 2);
		face = image(Rect(faces[i].x, faces[i].y, faces[i].width, faces[i].height));
	}


	//	imshow("2",face);
	//	cvWaitKey(0);
}

void rotateImgDemo()
{
	cv::Mat srcImg = cv::imread(filePath);

	if (!srcImg.data) return ;

	cv::imshow("srcImg", srcImg);

	cv::Point2f center =
			cv::Point2f(srcImg.cols / 2, srcImg.rows / 2);

	double angle = 60;//旋转角度

	double scale = 1;

//	cv::Mat rotateImage;
	Mat rotateImage(srcImage.size(), srcImage.type());
	rotateImage = cv::getRotationMatrix2D(center, angle, scale);//获得一个仿射矩阵
	imshow("rotateImage", rotateImage);

	cv::Mat rotateImg;
	cv::warpAffine(srcImg, rotateImg, rotateImage, srcImg.size());

	//cv::imshow("rotateImage", rotateImg);

	cv::waitKey(0);
}

void funcOfMatTest()
{
	cv::Mat Image1(10, 8, CV_8UC1, cv::Scalar(5));

	cout << "Image1 row:" << Image1.rows << endl;
	cout << "Image1.col:" << Image1.cols << endl;

	cout << Image1.rowRange(1, 3) << endl;
	cout << Image1.colRange(2, 4) << endl;

	cv::Mat Image2(8, 8, CV_32FC2, cv::Scalar(1, 5));
	Image2.create(10, 10, CV_8UC(3));
	cout << "Image2 channels:" << Image2.channels() << endl;

	//转换矩阵类型
	Image2.convertTo(Image2, CV_32F);
	cout << "Image2 depth:" << Image2.depth() << endl;

	//zeros创建矩阵
	cv::Mat Image3 = cv::Mat::zeros(Image2.rows, Image2.cols, CV_8UC1);

}

void imgReadDisplaySave()
{
	cv::Mat srcImage = cv::imread("G:\\pic.jpg");
	if (srcImage.empty()) {
		cout << "read img failed.\n";
		return;
	}

	//转为灰度图像
	Mat srcGray;
	cvtColor(srcImage, srcGray, CV_RGB2GRAY);
	imshow("srcGray", srcGray);
	//均值平滑
	Mat blurDstImage;
	blur(srcGray, blurDstImage, Size(5, 5), Point(-1, -1));
	imshow("blurDstImage", blurDstImage);
	//写入图像文件
	imwrite("blurDstImage.png", blurDstImage);
	waitKey(0);

}

Mat myRotateImg(Mat &srcImage, int angle)
{
	Mat destImage(srcImage.size(), srcImage.type());//复制原图的尺寸和类型

	//计算出中心点
	int centerX = -srcImage.rows/2;
	int centerY = -srcImage.cols/2;
	//遍历每个像素点
	for(int i = 0; i < srcImage.rows; i++){
		for(int j = 0; j < srcImage.cols; j++){

		}

	}
	return destImage;
}

void remapTest()
{
	Mat srcImage = imread(filePath);
	if(!srcImage.data){
		cout<<"read img failed.\n";
		return ;
	}

	//    cout<<srcImage.type();
	//输出矩阵定义
	Mat resultImage(srcImage.size(), srcImage.type());
	//x与y方向矩阵
	Mat xMapImage(srcImage.size(), CV_32FC1);
	Mat yMapImage(srcImage.size(), CV_32FC1);
	//取图像的宽高
	int rows = srcImage.rows;
	int cols = srcImage.cols;
	//图像遍历
	for(int j = 0; j<rows; j++){
		for(int i = 0; i < cols; i++){
			//x与y均翻转
			xMapImage.at<float>(j, i) = cols - i;
			//            yMapImage.at<float>(j, i) = rows - j;
			yMapImage.at<float>(j, i) = j;
		}
	}

	//重映射操作
	remap(srcImage, resultImage, xMapImage, yMapImage,
			CV_INTER_LINEAR, BORDER_CONSTANT, Scalar(0,0,0));
	//输出结果
	imshow("srcImage", srcImage);
	imshow("resultImage", resultImage);
	waitKey(0);
}

//平移操作，图片大小不变
Mat imageTranslation1(Mat & srcImage, int xoffset, int yoffset)
{
	int nRows = srcImage.rows;
	int nCols = srcImage.cols;

	Mat resultImage(srcImage.size(), srcImage.type());//构建一个只有大小和类型图片，没有具体内容


	//遍历图像
	for(int i = 0; i < nRows; ++i){
		for(int j = 0; j < nCols; ++j){
			//映射变换
			int x = j + xoffset;
			int y = i + yoffset;
			//边界判断
			if(x >= 0 && y >= 0 && x < nCols && y < nRows){
				//                resultImage.at<Vec3b>(i, j) = srcImage.ptr<Vec3b>(y)[x];
				resultImage.at<Vec3b>(y, x) = srcImage.ptr<Vec3b>(i)[j];
			}
		}
	}
	return resultImage;
}

//平移操作，图片大小改变
Mat imageTranslation2(Mat & srcImage, int xOffset, int yOffset)
{
	//设置平移尺寸
	int nRows = srcImage.rows + abs(yOffset);
	int nCols = srcImage.cols + abs(xOffset);

	Mat resultImage(nRows, nCols, srcImage.type());//构建一个只有大小和类型图片，没有具体内容

	//遍历图像
	for(int i = 0; i < nRows; ++i){
		for(int j = 0; j < nCols; ++j){
			//映射变换
			int x = j + xOffset;
			int y = i + yOffset;
			//边界判断
			if(x >= 0 && y >= 0 && x < nCols && y < nRows){
				//                resultImage.at<Vec3b>(i, j) = srcImage.ptr<Vec3b>(y)[x];
				resultImage.at<Vec3b>(y, x) = srcImage.ptr<Vec3b>(i)[j];
			}
		}
	}
	return resultImage;
}

void imgTranslationTest()
{
	Mat srcImage = imread(filePath);

	imshow("srcImage", srcImage);
	int xOffset = 50, yOffset = 0;
	//图像右平移不改变大小
	Mat resultImage1 = imageTranslation1(srcImage, xOffset, yOffset);
	imshow("resultImage1", resultImage1);

	//图像右平移改变大小
	Mat resultImage2 = imageTranslation2(srcImage, xOffset, yOffset);
	imshow("resultImage2", resultImage2);

	waitKey(0);
}

//基于等间隔提取图像缩放
void imageReduction1()
{
	Mat srcImage = imread(filePath);
	float kx = 0.9f;
	float ky = 0.9f;

	//    int i = 1;
	//    cout<<static_cast<int>((i+1) / kx + 0.5)<<endl;
	int nRows = cvRound(srcImage.rows * kx);
	int nCols = cvRound(srcImage.cols * ky);
	Mat resultImage(nRows, nCols, srcImage.type());
	for(int i = 0; i < nRows; ++i){
		for(int j = 0; j < nCols; ++j){
			//根据水平因子计算坐标
			//            int x = static_cast<int>((i+1) / kx + 0.5) - 1;//书上的算法(图像插值与采样)
			int x = i / kx;//我自己的算法

			//根据垂直因子计算坐标
			//            int y = static_cast<int>((j+1) / ky + 0.5) - 1;
			int y = j / ky;
			resultImage.at<Vec3b>(i, j) = srcImage.at<Vec3b>(x, y);
		}
	}
	imshow("srcImage", srcImage);
	imshow("resultImage", resultImage);
	waitKey(0);
}

//计算区域平均值
Vec3b areaAverage(const Mat &srcImage, Point_<int> leftPoint, Point_<int> rightPoint)
{
	int temp1 = 0;
	int temp2 = 0;
	int temp3 = 0;
	//计算区域子块像素点个数
	int nPix = (rightPoint.x - leftPoint.x + 1) * (rightPoint.y - leftPoint.y + 1);
	//对区域子块各个通道像素值求和
	for(int i = leftPoint.x; i <= rightPoint.x; i++){
		for(int j = leftPoint.y; j <= rightPoint.y; j++){
			temp1 += srcImage.at<Vec3b>(i, j)[0];
			temp2 += srcImage.at<Vec3b>(i, j)[1];
			temp2 += srcImage.at<Vec3b>(i, j)[2];
		}
	}
	//对每个通道求平均值
	Vec3b vecTemp;
	vecTemp[0] = temp1 / nPix;
	vecTemp[1] = temp2 / nPix;
	vecTemp[2] = temp3 / nPix;
	return vecTemp;
}

void imageReduction2()
{
	Mat srcImage = imread(filePath);
	double kx = 0.5;
	double ky = 0.5;

	//获取输出图像分辨率
	int nRows = cvRound(srcImage.rows * kx);
	int nCols = cvRound(srcImage.cols * ky);
	Mat resultImage(nRows, nCols, srcImage.type());
	//区域子块的左上角行列坐标
	int leftRowCoordinate = 0;
	int leftColCoordinate = 0;
	for(int i = 0; i < nRows; ++i){
		//根据水平因子计算坐标
		int x = static_cast<int>((i+1)/kx + 0.5) - 1;
		for(int j = 0; j < nCols; ++j){

			//根据垂直因子计算坐标
			int y = static_cast<int>((j+1) / ky + 0.5) - 1;
			//            int y = j / ky;
			//求解区域子块的均值
			resultImage.at<Vec3b>(i, j) =
					areaAverage(srcImage, Point_<int>(leftRowCoordinate, leftColCoordinate), Point_<int>(x, y));
			//更新下子块左上角的列坐标，行坐标不变
			leftColCoordinate = y + 1;
		}
		leftColCoordinate = 0;
		//更新下子块左上角的行坐标
		leftRowCoordinate = x + 1;
	}
	imshow("srcImage", srcImage);
	imshow("resultImage", resultImage);
	waitKey(0);
}

Mat angleRotate(Mat &src, int angle)
{
	//角度转换
	float alpha = angle * CV_PI / 180;
	//构造旋转矩阵
	float rotateMat[3][3] = {
			{cos(alpha), -sin(alpha), 0},
			{sin(alpha), cos(alpha), 0},
			{0, 0, 1}
	};
	int nSrcRows = src.rows;
	int nSrcCols = src.cols;
	//计算旋转后图像矩阵的各个顶点的位置
	float a1 = nSrcCols * rotateMat[0][0];
	float b1 = nSrcCols * rotateMat[1][0];
	float a2 = nSrcCols * rotateMat[0][0] + nSrcRows * rotateMat[0][1];
	float b2 = nSrcCols * rotateMat[1][0] + nSrcRows * rotateMat[1][1];
	float a3 = nSrcRows * rotateMat[0][1];
	float b3 = nSrcRows * rotateMat[1][1];
	//计算出极值点
	float kxMin = min(min(min(0.0f, a1), a2), a3);
	float kxMax = max(max(max(0.0f, a1), a2), a3);
	float kyMin = min(min(min(0.0f, b1), b2), b3);
	float kyMax = max(max(max(0.0f, b1), b2), b3);
	//计算输出矩阵的尺寸
	int nRows = abs(kxMax - kxMin);
	int nCols = abs(kyMax - kyMin);
	Mat dst(nRows, nCols, src.type(), Scalar::all(0));
	for(int i = 0; i < nRows; i++){
		for(int j = 0; j < nCols; j++){
			//旋转坐标转换
			int x = (j + kxMin) * rotateMat[0][0] - (i + kyMin) * rotateMat[0][1];
			int y = -(j + kxMin) * rotateMat[1][0] + (i + kyMin) * rotateMat[1][1];
			//区域旋转
			if((x >= 0) && (x < nSrcCols) && (y >= 0) && (y < nSrcRows)){
				dst.at<Vec3b>(i, j) = src.at<Vec3b>(y, x);
			}
		}
	}
	return dst;
}

void imgRotateTest()
{
	Mat srcImage = imread(filePath);
	imshow("srcImage", srcImage);
	//int angle = 180;
	//    Mat resultImage = angleRotate(srcImage, angle);
	Mat resultImage;
	//    transpose(srcImage, resultImage);//逆时针旋转90度

	flip(srcImage, resultImage, 1);//水平翻转（绕水平线翻转

	imshow("resultImage", resultImage);
	waitKey(0);

}


//仿射变换测试
void affineTranslation()
{
	Mat srcImage = imread(filePath);
	imshow("srcImage", srcImage);
	int nRows = srcImage.rows;
	int nCols = srcImage.cols;
	//定义仿射变换的二维点数组
	//源图像和目标图像对应映射的三个点
	Point2f srcPoint[3];
	Point2f resPoint[3];
	srcPoint[0] = Point2f(0, 0);
	srcPoint[1] = Point2f(nCols -1, 0);
	srcPoint[2] = Point2f(0, nRows - 1);
	resPoint[0] = Point2f(nCols * 0, nRows * 0.33);
	resPoint[1] = Point2f(nCols * 0.85, nRows * 0.25);
	resPoint[2] = Point2f(nCols * 0.15, nRows * 0.7);

	//定义仿射变换矩阵2 x 3
	Mat warpMat(Size(2, 3), CV_32F);
	Mat resultImage = Mat::zeros(nRows, nCols, srcImage.type());
	//计算仿射变换矩阵， 即仿射变换的2 x 3数组
	warpMat = getAffineTransform(srcPoint, resPoint);
	//根据仿射矩阵计算图像仿射变换
	warpAffine(srcImage, resultImage, warpMat, resultImage.size());
	imshow("resultImage", resultImage);
	//设置仿射变换参数
	Point2f centerPoint = Point2f(nCols/2, nRows/2);
	double angle = -50;
	double scale = 0.7;//尺寸
	//获取仿射变换矩阵
	warpMat = getRotationMatrix2D(centerPoint, angle, scale);
	//对原图像角度仿射变换
	warpAffine(srcImage, resultImage, warpMat, resultImage.size());
	imshow("resultImage", resultImage);
	waitKey(0);

}

void useCamera()
{
	VideoCapture capture(0);
	capture.set(CV_CAP_PROP_FRAME_WIDTH, 1280);
	capture.set(CV_CAP_PROP_FRAME_HEIGHT, 720);
	capture.open(0);
	if(!capture.isOpened()){
		cout<<"fail to open camera!"<<endl;
		return ;
	}

	Mat frameImg;
	//read方法获取显示帧
	long nCount = 1;
	while(true){
		//输出当前帧
		cout<<"Current frame: "<<nCount<<endl;

		capture>>frameImg;
		//判断当前读取文件
		if(!frameImg.empty()){
			imshow("frameImg", frameImg);
			//            QThread::usleep(40000);
		}else{
			break;
		}
		//按下键盘的Q键退出
		if(char(waitKey(1)) == 'q'||char(waitKey(1)) == 'Q'){
			break;
		}
		nCount++;
	}
	capture.release();
}

void videoCaptureTest()
{
	VideoCapture capture;
	//读取视频文件
	capture.open(videoPath);
	if(!capture.isOpened()){
		cout<<"fail to open video!"<<endl;
		return ;
	}
	//获取视频相关信息——帧数
	long nTotalFrame = capture.get(CV_CAP_PROP_FRAME_COUNT);
	cout<<"nTotalFrame = "<<nTotalFrame<<endl;
	//获取帧像素宽/高
	int frameHeight = capture.get(CV_CAP_PROP_FRAME_HEIGHT);
	int frameWidth = capture.get(CV_CAP_PROP_FRAME_WIDTH);
	cout<<"frameHeight = "<<frameHeight<<endl;
	cout<<"frameWidth = "<<frameWidth<<endl;
	//获取帧率
	double FrameRate = capture.get(CV_CAP_PROP_FPS);
	cout<<"FrameRate = "<<FrameRate<<endl;
	//    return ;
	Mat frameImg;
	//read方法获取显示帧
	long nCount = 1;
	while(true){
		//输出当前帧
		cout<<"Current frame: "<<nCount<<endl;

		capture>>frameImg;
		//判断当前读取文件
		if(!frameImg.empty()){
			imshow("frameImg", frameImg);
			//sleep(40000);
		}else{
			break;
		}
		//按下键盘的Q键退出
		if(char(waitKey(1)) == 'q'||char(waitKey(1)) == 'Q'){
			break;
		}
		nCount++;
	}
	capture.release();

}

void writeVideo()
{
	string sourceVideoPath = videoPath;
	string outputVideoPath = "D:/Users/Administrator/Videos/testWrite.avi";
	//视频输入
	VideoCapture inputVideo(sourceVideoPath);
	//检测视频输入的有效性
	if(!inputVideo.isOpened()){
		cout<<"fail to open!"<<endl;
		return ;
	}
	VideoWriter outputVideo;
	Size videoResolution = Size((int)inputVideo.get(CV_CAP_PROP_FRAME_WIDTH), (int)inputVideo.get(CV_CAP_PROP_FRAME_HEIGHT));
	//double fps = inputVideo.get(CV_CAP_PROP_FPS);
	//open方法相关设置
	outputVideo.open(outputVideoPath, -1, 25.0, videoResolution, true);
	if(!outputVideo.isOpened()){
		cout<<"fail to open!"<<endl;
		return ;
	}
	Mat frameImg;
	int count = 0;
	//vector RGB分量
	vector<Mat> rgb;
	Mat resultImg;
	while(true){
		inputVideo>>frameImg;
		//视频帧结束判断
		if(!frameImg.empty()){
			count++;
			imshow("frameImg", frameImg);
			//分离三通道rgb
			split(frameImg, rgb);
			cout<<"rgb size = "<<rgb.size()<<endl;
			for(int i = 0; i < 3; i++){
				if(i != 0){
					//提取B通道分量
					rgb[i] = Mat::zeros(videoResolution, rgb[0].type());
				}
				//通道合并
				merge(rgb, resultImg);
			}
			imshow("resultImg", resultImg);
			outputVideo<<resultImg;
			//            outputVideo<<frameImg;
		}

		else{
			break;
		}
		//按下Q键退出
		if(char(waitKey(1)) == 'q'||char(waitKey(1)) == 'Q'){
			break;
		}
		cout<<"writeTotalFrame: "<<count<<endl;
	}
}

double PSNR(const Mat & I1, const Mat & I2)
{
	Mat s1;
	//计算图像差|I1 - I2|
	absdiff(I1, I2, s1);
	//转成32位浮点数进行平方运算
	s1.convertTo(s1, CV_32F);
	//s1*s1，即|I1 - I2|^2
	s1 = s1.mul(s1);
	//分别叠加每个通道的元素，存于s中
	Scalar s = sum(s1);
	//计算所有通道元素和
	double sse = s.val[0] + s.val[1] + s.val[2];
	//当元素很小时返回0值
	if(sse <= 1e-10){
		return 0;
	}

	else{
		//根据公式计算I1与I2的均方误差
		double mse = sse / (double)(I1.channels() * I1.total());
		//计算峰值信噪比
		double psnr = 10.0*log10((255*255)/mse);
		return psnr;
	}
}

Scalar MSSIM(const Mat & i1, const Mat & i2)
{
	const double c1 = 6.5025, c2 = 58.5225;
	Mat I1, I2;
	//转换成32位浮点数进行平方运算
	i1.convertTo(I1, CV_32F);
	i2.convertTo(I2, CV_32F);
	//I2^2
	Mat I2_2 = I2.mul(I2);
	Mat I1_2 = I1.mul(I1);
	Mat I1_I2 = I1.mul(I2);
	Mat mu1, mu2;
	//高斯加权计算每一窗口的均值、方差以及协方差
	GaussianBlur(I1, mu1, Size(11, 11), 1.5);
	GaussianBlur(I2, mu2, Size(11, 11), 1.5);
	Mat mu1_2 = mu1.mul(mu1);
	Mat mu2_2 = mu2.mul(mu2);
	Mat mu1_mu2 = mu1.mul(mu2);
	Mat sigma1_2, sigma2_2, sigma12;
	//高斯平滑，具体原理见4.4节
	GaussianBlur(I1_2, sigma1_2, Size(11, 11), 1.5);
	sigma1_2 -= mu1_2;
	GaussianBlur(I2_2, sigma2_2, Size(11, 11), 1.5);
	sigma2_2 -= mu2_2;
	GaussianBlur(I1_I2, sigma12, Size(11, 11), 1.5);
	sigma12 -= mu1_mu2;
	//根据公式计算相应的参数
	Mat t1, t2, t3;
	t1 = 2*mu1_mu2 + c1;
	t2 = 2 * sigma12 + c2;
	t1 = t1.mul(t2);
	Mat ssim_map;
	divide(t3, t1, ssim_map);
	//将平均值作为两图像的结构相似性度量
	Scalar mssim = mean(ssim_map);
	return mssim;
}

void trackbarTest()
{
	Mat srcImage = imread(filePath);
	if(!srcImage.data) return ;

	//转换为灰度图像
	Mat srcGray;
	cvtColor(srcImage, srcGray, CV_RGB2GRAY);
	namedWindow("dyn_threshold");
	imshow("dyn_threshodl", srcGray);
	//创建滑动条createTrackbar, 调用回掉函数
	createTrackbar("pos", "dyn_threshold", 0, 255, onChangeTrackBar, &srcGray);
	waitKey(0);

}

void onChangeTrackBar(int pos, void *data)
{
	//强制类型转换
	Mat srcImage = *(Mat *)(data);
	Mat dstImage;
	//根据滑动条的值进行二值化
	threshold(srcImage, dstImage, pos, 255, 0);
	imshow("dyn_threshold", dstImage);

}

void regionExtraction(int xRoi, int yRoi, int widthRoi, int heightRoi)
{
	//提取指定坐标区域
	Mat roiImage(srcImage.rows, srcImage.cols, CV_8UC3);
	cout<<srcImage.rows<<" "<<srcImage.cols<<endl;
	//将兴趣区域复制到目标图像
	srcImage(Rect(xRoi, yRoi, widthRoi, heightRoi)).copyTo(roiImage);
	imshow("roiImage", roiImage);
	waitKey(0);
}

void regionExtractionTest()
{
	srcImage = imread(filePath);
	imshow("ROIing", srcImage);
	waitKey(0);
	//方法1:利用Rect选择区域(100, 180, 150, 50)
	int xRoi = 80;
	int yRoi = 180;
	int widthRoi = 150;
	int heightRoi = 100;
	regionExtraction(xRoi, yRoi, widthRoi, heightRoi);
}

void mouseEvent(int event, int x, int y, int flags, void *data)
{
	//鼠标左键按下，获取起始点
	if(event == EVENT_LBUTTONDOWN){
		downFlag = true;
		startPoint.x = x;
		startPoint.y = y;
	}
	//鼠标拖到松开，获取终止点
	if(event == EVENT_LBUTTONUP){
		upFlag = true;
		endPoint.x = x;
		endPoint.y = y;
	}

	//显示拖到图像区域
	if(downFlag == true && upFlag == false){
		Point tempPoint;
		tempPoint.x = x;
		tempPoint.y = y;
		//用于显示图像生成
		Mat tempImage = srcImage.clone();
		rectangle(tempImage, startPoint, endPoint, Scalar(255, 0, 0), 2, 3, 0);
		imshow("ROIing", tempImage);
	}
	//选择区域生成
	if(downFlag == true && upFlag == true){
		//获取选择区域的ROI
		roiRect.width = abs(startPoint.x - endPoint.x);
		roiRect.height = abs(startPoint.y - endPoint.y);
		roiRect.x = min(startPoint.x, endPoint.x);
		roiRect.y = min(startPoint.y, endPoint.y);
		Mat roiMat(srcImage, roiRect);
		imshow("ROI", roiMat);
		downFlag = false;
		upFlag = false;
	}
	waitKey(0);
}

void mouseEventTest()
{
	srcImage = imread(filePath);
	//回调事件相应
	namedWindow("srcImage");
	setMouseCallback("srcImage", mouseEvent, 0);
	imshow("srcImage", srcImage);
	waitKey(0);
}
