//============================================================================
// Name        : mySqlTest.cpp
// Author      : zhakangning
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include "mysql/mysql.h"

using namespace std;

int main() {
	// 创建数据连接对象
	MYSQL *con = mysql_init(NULL);
	if (con == NULL) {
		fprintf(stderr, "%s\n", mysql_error(con));
		exit(EXIT_FAILURE);
	}

	if (!mysql_real_connect(con, "localhost", "admin", "admin", "firstDB", 0, NULL, 0)) {
		fprintf(stderr, "%s\n", mysql_error(con));
		mysql_close(con);
		exit(EXIT_FAILURE);
	}


	if (mysql_query(con, "show databases;")) {
		fprintf(stderr, "%s\n", mysql_error(con));
		mysql_close(con);
		exit(EXIT_FAILURE);
	}

	MYSQL_RES *ret = mysql_store_result(con);

	 MYSQL_ROW row = mysql_fetch_row(ret);
	puts("mariadb is connect and run succesed!");
	mysql_close(con);
	return 0;
}
