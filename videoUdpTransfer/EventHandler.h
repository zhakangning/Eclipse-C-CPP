/*
 * CEventHandler.h
 *
 *  Created on: 2018年3月21日
 *      Author: root
 */

#ifndef EVENTHANDLER_H_
#define EVENTHANDLER_H_

typedef int TSocketHandle;

class CEventHandler {
public:
	CEventHandler();
	virtual ~CEventHandler();

public:
	virtual void HandleInput();
	virtual void HandleOutput();
	virtual void HandleError();
	virtual TSocketHandle GetHandle();

protected:
	TSocketHandle m_nHandle;

};

#endif /* EVENTHANDLER_H_ */
