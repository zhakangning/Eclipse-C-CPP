/*
 * CEventManager.h
 *
 *  Created on: 2018年3月21日
 *      Author: root
 */

#ifndef EVENTMANAGER_H_
#define EVENTMANAGER_H_

#include <sys/epoll.h>

#define EVENT_READ EPOLLIN
#define EVENT_WRITE EPOLLOUT
#define EVENT_ERROR EPOLLERR

class CEventHandler;

class CEventManager {
public:
	CEventManager();
	virtual ~CEventManager();

public:
	int Open(int nNumber);
	int Close();

	int AddEvent(CEventHandler* handler, unsigned int e);
	int RemoveEvent(CEventHandler* handler);

	int StartLoop();
	int StopLoop();

private:
	int m_run;
	int m_epfd;
	int m_ecount;
	epoll_event* m_events;
	epoll_event m_ev;

};

#endif /* EVENTMANAGER_H_ */
