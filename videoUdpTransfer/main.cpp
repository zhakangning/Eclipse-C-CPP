/*
 * main.cpp
 *
 *  Created on: 2018年3月21日
 *      Author: root
 */

#include "udpTransfer.h"
#include "EventManager.h"

int main()
{
	int sockFd = socket(AF_INET, SOCK_DGRAM, 0);
	if(sockFd == -1){
		perror("socket");
		return -1;
	}

	//设置为非阻塞
	int opt = SOCK_NONBLOCK;
	setsockopt(sockFd, SOL_SOCKET, SOCK_NONBLOCK, &opt, sizeof(opt));

	struct sockaddr_in local;
	local.sin_family = AF_INET;
	local.sin_port = htons(8888);
	local.sin_addr.s_addr = INADDR_ANY;
	if(bind(sockFd, (struct sockaddr*)&local, sizeof(local))<0)
	{
		perror("bind");
		return -1;
	}

	CudpTransfer udpTransfer(sockFd);
	CEventManager eventMng;
	eventMng.Open(10);
	eventMng.AddEvent(&udpTransfer, EVENT_READ);
	eventMng.StartLoop();
	return 0;
}


