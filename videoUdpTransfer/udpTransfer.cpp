/*
 * CudpTransfer.cpp
 *
 *  Created on: 2018年3月21日
 *      Author: root
 */

#include "udpTransfer.h"

CudpTransfer::CudpTransfer(int sockFd) {
	// TODO Auto-generated constructor stub
	m_nHandle = sockFd;
}

CudpTransfer::~CudpTransfer() {
	// TODO Auto-generated destructor stub
}

void CudpTransfer::HandleInput()
{
	int sockFd = m_nHandle;
	char buf[1024];
	struct sockaddr_in client;
	socklen_t len = sizeof(client);
	int r = recvfrom(sockFd, buf, sizeof(buf)-1, 0, (struct sockaddr*)&client, &len);
	if(r < 0)
	{
		perror("recvfrom");
		return ;
	}

	buf[r] = 0;
	printf("[%s : %d]#  %s\n",inet_ntoa(client.sin_addr), ntohs(client.sin_port), buf+6);

	//取出内容
	struct sockaddr_in dest;
	dest.sin_addr = *(struct in_addr *)buf;
	dest.sin_port = *(short *)(buf + 4);
	cout<<"ip = "<<inet_ntoa(dest.sin_addr)<<endl;
	cout<<"port = "<<ntohs(dest.sin_port)<<endl;

	//填充内容
	*(struct in_addr *)buf = client.sin_addr;
	*(short *)(buf+4) = client.sin_port;

	//转发数据
	sendto(sockFd, buf, strlen(buf), 0, (struct sockaddr*)&dest, len);
}
