/*
 * CudpTransfer.h
 *
 *  Created on: 2018年3月21日
 *      Author: root
 */

#ifndef UDPTRANSFER_H_
#define UDPTRANSFER_H_
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <stdio.h>
#include<arpa/inet.h>
#include <string.h>
#include <iostream>
#include <pthread.h>

#include "EventHandler.h"

using namespace std;

class CudpTransfer :public CEventHandler{
public:
	CudpTransfer(int sockFd);
	virtual ~CudpTransfer();
	void HandleInput();

};

#endif /* UDPTRANSFER_H_ */
