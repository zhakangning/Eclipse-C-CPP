/*
 * CEventManager.cpp
 *
 *  Created on: 2018年3月21日
 *      Author: root
 */

#include "EventManager.h"

#include <stdlib.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/time.h>

#include "EventHandler.h"

CEventManager::CEventManager()
:m_run(1),
 m_epfd(-1),
 m_ecount(0),
 m_events(0)
{
}

CEventManager::~CEventManager() {
	// TODO Auto-generated destructor stub
	Close();
}

int CEventManager::Open(int nNumber)
{
	m_ecount = nNumber;
	m_events = new epoll_event[nNumber];
	m_epfd = epoll_create(nNumber);
	return 0;
}
int CEventManager::Close()
{
	if(m_events){
		delete []m_events;
		m_events = NULL;
	}

	if(m_epfd != -1){
		close(m_epfd);
		m_epfd = -1;
	}
	return 0;
}

int CEventManager::AddEvent(CEventHandler* handler, unsigned int e)
{
	m_ev.data.ptr = handler;
	m_ev.events = e;
	epoll_ctl(m_epfd, EPOLL_CTL_ADD, handler->GetHandle(), &m_ev);
	return 0;
}

int CEventManager::RemoveEvent(CEventHandler* handler)
{
	m_ev.data.ptr = handler;
	epoll_ctl(m_epfd, EPOLL_CTL_DEL, handler->GetHandle(), &m_ev);
	return 0;
}

int CEventManager::StartLoop()
{
	int nfds, i;
	m_run = 1;
	while(m_run)
	{
		nfds = epoll_wait(m_epfd, m_events, m_ecount, -1);
		for(i = 0; i < nfds; i++){
			CEventHandler* handle = (CEventHandler*)m_events[i].data.ptr;
			if(m_events[i].events & EPOLLIN){
				handle->HandleInput();
			}else if(m_events[i].events & EPOLLOUT){
				handle->HandleOutput();
			}else if(m_events[i].events & EPOLLERR){
				handle->HandleError();
			}
		}
	}

	Close();
	return 0;
}
int CEventManager::StopLoop()
{
	m_run = 0;
	return 0;
}

