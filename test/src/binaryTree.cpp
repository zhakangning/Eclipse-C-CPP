#include "define.h"

//队列长度
#define LEN 16

//初始化队列
//unsigned int count=0;//表示有队列有几个元素

//先序生成二叉树
BTREE *createBTree(int *a)
{
	static int i=0;
	if(a[i++]==0){
		return NULL;
	}

	BTREE *bt=(BTREE *)malloc(sizeof(BTREE));
	bt->data=a[i-1];
	bt->left=createBTree(a);
	bt->right=createBTree(a);
	return bt;
}

//先序遍历二叉树
void preOrderVisit(BTREE *rootNode)
{
	if(rootNode){
		cout<<rootNode->data;
		preOrderVisit(rootNode->left);
		preOrderVisit(rootNode->right);
	}else{
		return ;
	}

}

void preOrderVisit(int *a, int rootIndex, int len)
{
	static int n = 0;
	cout<<n++<<":"<<a[rootIndex]<<endl;

	//没有左孩子，退出函数
	if(2*rootIndex + 1 > len -1) return ;
	//遍历左子树
	preOrderVisit(a, 2*rootIndex + 1, len);

	//没有右孩子，退出函数
	if(2*rootIndex + 2 > len -1) return ;
	//遍历右子树
	preOrderVisit(a, 2*rootIndex + 2, len);
}

void preOrderVisitByStack(BTREE *rootNode)
{
	BTREE *p = rootNode;
	stack<BTREE *> tempStack;
	while(true){
		cout<<p->data;//打印根结点数据
		if(p->left != NULL){
			tempStack.push(p);//把带有左孩子节点的节点入栈
			p = p->left;
		}

		else if(p->right != NULL){
			p = p->right;
		}

		else if(tempStack.size() != 0){//如果左右孩子都没有，就出栈一个节点
			p = tempStack.top()->right;//由于入栈的节点已经判断过左孩子了，所以直接把该节点的右孩子赋给p
			tempStack.pop();
		}

		else break;
	}
}

void preOrderVisitByStack(int *a, int len)
{
	static int n = 0;
	int rootIndex = 0;
	stack<int> tempStk;
	while(true){

		cout<<n++<<":"<<a[rootIndex]<<endl;
		if(2*rootIndex + 1 <= len -1){
			tempStk.push(rootIndex);
			rootIndex = 2*rootIndex + 1;
		}

		else if(2*rootIndex + 2 <= len -1){
			rootIndex = 2*rootIndex + 2;
		}

		else if(tempStk.size() != 0){
			rootIndex = tempStk.top()*2 + 2;//由于入栈的节点已经判断过左孩子了，所以直接把该节点的右孩子下标赋给rootIndex
			tempStk.pop();
		}

		else break;
	}
}

//中序遍历二叉树
void inOrderVisit(BTREE *rootNode)
{
	if(rootNode){
		inOrderVisit(rootNode->left);
		printf("%d\n",rootNode->data);
		inOrderVisit(rootNode->right);
	}else{
		return ;
	}

}

//后序遍历二叉树
void postOrderVisit(BTREE *rootNode)
{
	if(rootNode){
		postOrderVisit(rootNode->left);
		postOrderVisit(rootNode->right);
		printf("%c\n",rootNode->data);
	}else{
		return ;
	}
}

//入队
//void enqueue(BTREE *node)
//{
//	if(!node)
//		return ;
//	//循环队列
//	if(rear==LEN)
//		rear=0;
//	qnode[rear++]=node;
////	count++;
//}

//出队
//BTREE *dequeue()
//{
//	if(front==LEN)
//		front=0;
////	count--;
//	return qnode[front++];
//}

//层次遍历二叉树
void levelVisit(BTREE *rootNode)
{
	if(NULL==rootNode)
		return;
	deque<BTREE *> queue;//用于层次遍历的队列
	BTREE *p;

	queue.push_back(rootNode);//把根节点入队
	while(queue.size()){

		p = queue.front();//队列第一个节点赋给p
		queue.pop_front();//把队列的一个元素出队
		if(NULL != p->left){
			queue.push_back(p->left);
		}
		if(NULL != p->right){
			queue.push_back(p->right);
		}
		cout<<p->data;
	}
}

//求二叉树的深度
int getDepth(BTREE *rootNode)
{
	if(rootNode==NULL){
		return 0;
	}

	int m,n;
	m=0;
	n=0;
	m=getDepth(rootNode->left)+1;
	n=getDepth(rootNode->right)+1;
	return m>n?m:n;
}

//通过二叉树的前序遍历和中序遍历生成二叉树
//参数：先序序列字符串、中序序列字符串、节点数
BTREE *createTreeByPreOrderAndInOrder(char *pre, char *in, int n)
{
	//先把根节点赋值
	BTREE *root = (BTREE *)malloc(sizeof(BTREE));
	root->data = *pre;
	//判断节点数
	if(n == 1){
		root->left=NULL;
		root->right=NULL;
		return root;
	}

	int i=0;
	while(in[i] != pre[0])
		i++;
	root->left=createTreeByPreOrderAndInOrder(pre + 1, in, i);
	root->right=createTreeByPreOrderAndInOrder(pre+i+1, in+i+1, i);
	return root;
}

//求某个节点的层数
int getLevel(BTREE *root ,char data)
{
	int lev,m,n;
	if(NULL==root)
		lev=0;
	else if(root->data == data)
		lev=1;
	else{
		m=getLevel(root->left,data);
		n=getLevel(root->right,data);
		if(m==0&&n==0) lev=0;
		else lev=(m>n?m:n)+1;
	}
	return lev;
}

BTREE * initBinaryTree()
{
	//构建一棵完全二叉树，层次遍历的顺序是ABCDEFG
	char pre[] = "ABDECFG";
	char in[] = "DBEAFCG";
	return createTreeByPreOrderAndInOrder(pre, in, 3);
}

void initBinaryTree(int *a, int n)
{
	for(int i = 0; i < n; i ++){
		a[i] = i;
	}
}
