/*
 * eightQueen.cpp
 *
 *  Created on: 2018年3月2日
 *      Author: root
 */

#include "define.h"

void solveEightQueen()
{
	int chessboard[8][8] = {0};
	int rstCount = 0;//答案计数
	stack<int> stack;
	int i = 0, j = 0;
	while(i > -1 && i < 8){
		while(j < 8){
			if(check(chessboard, i, j)){
				//检查结果可以落子
				chessboard[i][j] = 1;//落子
				//				printChessboard(chessboard);
				stack.push(j);//存下落子的列位置
				j = 0;//落子成功，直接把列下标置为0，表示下一行落子的时候从头开始检查
				break;//退出列的循环，进入下一行
			}
			j++;
		}
		if(j == 8){
			if(i == 0) return ;//第一行已经没有地方可以落子了，说面遍历完了，可以结束了

			//该行没有落子的地方，回溯到上一次落子的地方，重新落子
			i--;
			j = stack.top();//回到上次落子位置的右边开始检查
			stack.pop();
			chessboard[i][j] = 0;//取消上次的落子
			//			printChessboard(chessboard);
			j++;//回到上次落子位置的右边开始检查
			continue;
		}
		//该行落子成功
		i++;
		if(i == 8){
			printChessboard(chessboard);
			rstCount++;
			cout<<"rstCount = "<<rstCount<<endl;
			//寻找到一种答案，开始寻找第二种答案
			i--;
			j = stack.top();//回到上次落子位置的右边开始检查
			stack.pop();
			chessboard[i][j] = 0;//取消上次的落子
			//			printChessboard(chessboard);
			j++;//回到上次落子位置的右边开始检查
		}
	}
	//	cout<<"rstCount = "<<rstCount<<endl;
}

bool check(int chessboard[][8], int row, int col)
{
	//检查该行有没有棋子
	for(int i = 0; i < 8; i++){
		if(chessboard[row][i] == 1) return false;
	}

	//检查该列有没有棋子
	for(int i = 0; i < 8; i++){
		if(chessboard[i][col] == 1) return false;
	}

	//检查 '\' 斜杠有没有棋子
	int row1 = row;
	int col1 = col;
	if(row < col){
		row1 = 0;
		col1 = col - row;
	}else{
		col1 = 0;
		row1 = row - col;
	}
	while(row1 != 7 && col1 != 7){
		if(chessboard[row1][col1] == 1) return false;
		row1 ++;
		col1 ++;
	}

	//检查 '/' 斜杠有没有棋子
	if(row-0 < 7-col){
		row1 = 0;
		col1 = col + (row - row1);
	}else{
		col1 = 7;
		row1 = row - (col1 - col);
	}
	while(row1 != 7 && col1 != 0){
		if(chessboard[row1][col1] == 1) return false;
		row1 ++;
		col1 --;
	}
	return true;
}

void markInChessboard(int chessboard[][8], int row, int col)
{
	//检查该行有没有棋子
	for(int i = 0; i < 8; i++){
		chessboard[row][i] = 2;
	}

	//检查该列有没有棋子
	for(int i = 0; i < 8; i++){
		chessboard[i][col] = 2;
	}

	//检查 '\' 斜杠有没有棋子
	int row1 = row;
	int col1 = col;
	if(row < col){
		row1 = 0;
		col1 = col - row;
	}else{
		col1 = 0;
		row1 = row - col;
	}
	while(row1 != 8 && col1 != 8){
		chessboard[row1][col1] = 2;
		row1 ++;
		col1 ++;
	}

	//检查 '/' 斜杠有没有棋子
	if(row-0 < 7-col){
		row1 = 0;
		col1 = col + (row - row1);
	}else{
		col1 = 7;
		row1 = row - (col1 - col);
	}
	while(row1 != 8 && col1 != -1){
		chessboard[row1][col1] = 2;
		row1 ++;
		col1 --;
	}
	chessboard[row][col] = 1;//当前位置标记为1，表示已经落子
}

void printChessboard(int chessboard[][8])
{
	for(int i = 0; i < 8; i++){
		for(int j = 0; j < 8; j++){
			cout<<chessboard[i][j]<<" ";
		}
		cout<<endl;
	}
	cout<<endl;
}

void setChess(int chessboard[][8], int row, int lastCol)
{
	if(row == 8){
		printChessboard(chessboard);
		return ;
	}

	int col = 0;
	if(chessboard[row][col] == 0){
		markInChessboard(chessboard, row, col);
		setChess(chessboard, row + 1, col);
	}

	else if(col + 1 < 8){
		setChess(chessboard, row, lastCol);
	}

	else if(row - 1 >= 0){
		//回溯上一次落子的地方
		chessboard[row -1][lastCol] = 2;//把上一次落子的地方标记为2，表示不能再落子
		setChess(chessboard, row - 1, lastCol);
	}
}


