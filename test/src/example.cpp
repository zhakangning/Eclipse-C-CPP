/*
 * example.cpp
 *
 *  Created on: 2018年2月13日
 *      Author: root
 */


#include "define.h"
#include <sys/types.h>          /* See NOTES */
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/epoll.h>

//函数模板的定义
template <typename T> inline int compare(const T &v1, const T &v2)
{
	if(v1 < v2) return -1;
	//	if(v1 > v2) return 1;
	if(v2 < v1) return 1;//使用尽可能少的操作符，匹配更多的类型，详情见C++ Primer第4版535页
	return 0;
}


//类模板的定义
template <class Type> class Queue {
public:
	Queue();
};


void printValue(char c)
{
	cout<<c<<endl;
}

int openServer(const unsigned short &port)
{
	int fd = socket(AF_INET, SOCK_STREAM, 0);
	if(-1 == fd){
		perror("socket");
		return -1;
	}

	int opt = SO_REUSEADDR;
	if(setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &opt, sizeof(opt))!= 0){
		perror("setsockopt");
		return -1;
	}

	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;
	addr.sin_port = htons(port);
	if(-1 == bind(fd, (struct sockaddr *)&addr, sizeof(struct sockaddr)))
	{
		perror("bind");
		return -1;
	}
	if(listen(fd, 24) == -1){
		perror("listen");
		return -1;
	}
	cout<<"server started, listen port "<<port<<endl;
	return fd;
}

int setnonblocking(int fd)
{
	int opt = SOCK_NONBLOCK;
	return setsockopt(fd, SOL_SOCKET, SOCK_NONBLOCK, &opt, sizeof(opt));
}

void do_use_fd(int fd)
{
	char buf[1024];
	int rtn = read(fd, buf, sizeof(buf));
	buf[rtn] = 0;
	cout<<buf<<endl;
}

void epollHandle(int epoollFd, struct epoll_event *events, int nFds, int listenFd)
{
	struct epoll_event ev;
	struct sockaddr_in local;
	socklen_t addrLen;
	for(int i = 0; i < nFds; i++){
		if(events[i].data.fd == listenFd){//如果事件buffer中的fd是监听端口的fd，就说明是有新的连接请求
			int connFd = accept(listenFd, (struct sockaddr *) &local, &addrLen);
			if(-1 == connFd){
				perror("accept");
				return ;
			}
			setnonblocking(connFd);//设置新的连接socket为非阻塞模式
			ev.events = EPOLLIN | EPOLLET;//设置可读和边缘触发Edge Triggered
			ev.data.fd = connFd;
			if (epoll_ctl(epoollFd, EPOLL_CTL_ADD, connFd, &ev) == -1) {//把建立连接的socket描述符添加到epoll实例
				perror("epoll_ctl: conn_sock");
				return ;
			}
		}else{//事件buffer中的描述符是已经连接socket的描述符，进行相关的处理
			do_use_fd(events[i].data.fd);
		}
	}
}

void selectTest()
{
	fd_set rfds;
	struct timeval tv;
	int retval;

	/* Watch stdin (fd 0) to see when it has input. */
	FD_ZERO(&rfds);//清空集合
	FD_SET(0, &rfds);//添加文件描述符到集合，0表示标准输入的文件描述符

	/* Wait up to five seconds. */
	tv.tv_sec = 5;
	tv.tv_usec = 0;

	retval = select(1, &rfds, NULL, NULL, &tv);
	/* Don't rely on the value of tv now! */

	cout<<"retval = "<<retval<<endl;
	if (retval == -1)
		perror("select()");
	else if (retval)
		printf("Data is available now.\n");
	/* FD_ISSET(0, &rfds) will be true. */
	else
		printf("No data within five seconds.\n");
}

void selectTest2()
{
	int listenFd = openServer(8888);
	fd_set read_fds, tmp_fds;
	FD_ZERO(&read_fds);
	FD_SET(STDIN_FILENO, &read_fds);
	FD_SET(listenFd, &read_fds);
	int maxfd = listenFd;

	char buff[1024];
	while (1)
	{
		struct timeval tv = {3, 0};
		tmp_fds = read_fds;
		int ret = select(maxfd+1, &tmp_fds, NULL, NULL, &tv);
		if (ret == -1)
		{
			perror("select");
			exit(EXIT_FAILURE);
		}
		else if (ret == 0)
		{
			printf("timeout...\n");
		}
		//有文件描述符准备好
		else
		{
			for (int i = 0; i <= maxfd; i++)
			{
				if (FD_ISSET(i, &tmp_fds))
				{
					//标准输入准备好
					if (i == STDIN_FILENO)
					{
						fgets(buff, sizeof(buff), stdin);
						printf("%s", buff);
					}
					//监听套接字准备好
					else if (i == listenFd)
					{
						//4.接受客户端链接请求
						struct sockaddr_in client_addr;
						socklen_t len = sizeof(client_addr);
						int connect_fd = accept(listenFd, (struct sockaddr*)&client_addr, &len);
						if (connect_fd == -1)
						{
							perror("accept");
							exit(EXIT_FAILURE);
						}
						printf("accept from:%s:%hu\n", inet_ntoa(client_addr.sin_addr), ntohs(client_addr.sin_port));
						FD_SET(connect_fd, &read_fds);
						maxfd = maxfd > connect_fd ? maxfd : connect_fd;
					}
					//以前添加进去的connect_fd准备好
					else
					{
						ret = recv(i, buff, sizeof(buff), 0);
						if (ret == -1)
						{
							perror("recv");
							break;
						}
						else if (ret == 0)//对方关闭套接字
						{
							printf("client shutdown!\n");
							FD_CLR(i, &read_fds);
							close(i);
							break;
						}
						else
						{
							printf("recv:%s", buff);
						}
					}
				}
			}
		}

	}

	close(listenFd);
}

void epollTest()
{
	int listen_sock = openServer(7777);//创建TCP服务器，监听一个端口
	if(listen_sock == -1) return ;

	int epollFd = epoll_create(10);//创建一个epoll实例，返回epoll描述符，参数已经没有实际作用了
	if (epollFd == -1) {
		perror("epoll_create");
		return ;
	}

#define MAX_EVENTS 10
	struct epoll_event ev, events[MAX_EVENTS];
	ev.data.fd = listen_sock;
	ev.events = EPOLLIN;
	//把监听的socket描述符添加到epoll实例
	if (epoll_ctl(epollFd, EPOLL_CTL_ADD, listen_sock, &ev) == -1) {
		perror("epoll_ctl: listen_sock");
		return ;
	}

	int nFds;
	while(true){
		//在epoll实例中等待事件，返回触发事件的数目
		nFds = epoll_wait(epollFd, events, MAX_EVENTS, -1);
		if (nFds == -1) {
			perror("epoll_wait");
			return ;
		}
		epollHandle(epollFd, events, nFds, listen_sock);//epoll事件处理
	}
}

void redisTest()
{
	struct timeval timeout;
	timeout.tv_sec = 3;
	timeout.tv_usec = 0;
	redisContext* m_pContext = redisConnectWithTimeout("127.0.0.1", 6379, timeout);

	if(!m_pContext || m_pContext->err){
		printf("init redis is erro code[%d], msg[%s]\n", m_pContext->err, m_pContext->errstr);
		return ;
	}

	redisReply* reply = (redisReply*)redisCommand(m_pContext, "GET %s", "zkn");
	cout<<reply->integer<<endl;
	cout<<reply->len<<endl;
	cout<<reply->str<<endl;

	freeReplyObject(reply);
}

void catch_signal(int sign)
{
	switch (sign)
	{
	case SIGINT:
		printf("ctrl + C 被执行了!\n");
		//exit(0);
		break;
	case SIGALRM:
		cout<<"闹钟信号\n";
		break;
	}
}

void signalTest()
{
	//注册终端中断信号
	signal(SIGINT, catch_signal);
	char tempc = 0;
	while ((tempc = getchar()) != 'a')
	{
		printf("tempc=%d\n", tempc);
		//sleep()
	}
	//恢复信号
	signal(SIGINT, SIG_DFL);
	while (1)
	{
		pause();
	}
	printf("game over!\n");
}

void alarmSignalTest()
{
	signal(SIGALRM, catch_signal);
	alarm(3);
	pause();
}

void udpTest()
{
	int sockFd = socket(AF_INET, SOCK_DGRAM, 0);
	struct sockaddr_in dest;
	dest.sin_family = AF_INET;
	dest.sin_port = htons(8888);
	dest.sin_addr.s_addr = inet_addr("192.168.2.110");


	char buf[1024] = "000000haha";
	*(int *)buf = inet_addr("192.168.2.110");
	*(short *)(buf+4) = htons(8888);
	socklen_t len = sizeof(dest);
	sendto(sockFd, buf, strlen(buf), 0, (struct sockaddr*)&dest, len);
}

//Student类的构造函数的实现
Student::Student(string name , int age , char sex , int score)
{
	this->name = name ;
	this->age = age ;
	this->sex = sex ;
	this->score = score ;
}
//Techer类的构造函数的实现
Techer::Techer(string name , int age , char sex , int score)
{
	this->name = name ;
	this->age = age ;
	this->sex = sex ;
	this->score = score ;
}

//打印Student类中的私有成员和Techer的私有成员
void Student::stu_print(Techer &tech)
{
	//用this指针访问本类的成员
	cout << this->name << endl ;
	cout << this->age << endl ;
	cout << this->sex << endl ;
	cout << this->score << endl ;
	//访问Techer类的成员
	cout << tech.name << endl ;
	cout << tech.age << endl ;
	cout << tech.sex << endl ;
	cout << tech.score << endl ;
}

void friendTest()
{
	Student stu1("YYX",24,'N',86);
	Techer t1("hou",40,'N',99);
	//	stu1.stu_print(t1);
	printInfo(t1);
}

void printInfo(Techer &t)
{
	cout<<t.age<<endl;
}

void* mytask(void *arg)
{
	printf("thread %d is working on task %d\n", (int)pthread_self(), *(int*)arg);
	sleep(1);
	free(arg);
	return NULL;
}

void threadPoolTest()
{
	threadpool_t pool;
	//初始化线程池，最多三个线程
	threadpool_init(&pool, 3);
	int i;
	//创建十个任务
	for(i=0; i < 20; i++)
	{
		int *arg = (int *)malloc(sizeof(int));
		*arg = i;
		threadpool_add_task(&pool, mytask, arg);

	}
	threadpool_destroy(&pool);
}

void *consumer(void *arg)
{
	int id = *(int *)arg;
	while(true){
		if(pthread_mutex_lock(&lock) != 0){
			cout<<id<<":consumer get lock error first.\n";
		}
		cout<<id<<":consumer get lock.\n";
		while(productQueue.size() < 1){
			cout<<id<<":no product and waitting...\n";
			pthread_cond_wait(&cond4c, &lock);
			cout<<id<<":consumer wake.\n";
		}
		productQueue.pop_front();
		cout<<id<<":consume..."<<productQueue.size()<<endl;
		pthread_cond_signal(&cond4p);
		pthread_mutex_unlock(&lock);
		cout<<id<<":consumer unlock.\n";

		sleep(1);
	}

	return NULL;
}

void *producer(void *arg)
{
	while(true)
	{
		pthread_mutex_lock(&lock);
		cout<<"producer get lock.\n";
		if(productQueue.size() > 10){
			cout<<"queue is full, producer is waitting...\n";
			pthread_cond_wait(&cond4p, &lock);//条件不满足就阻塞，并且释放锁
			cout<<"producer wake.\n";
		}
		productQueue.push_back(0);
		cout<<"produce..."<<productQueue.size()<<endl;
		pthread_cond_signal(&cond4c);
		pthread_mutex_unlock(&lock);
		cout<<"producer unlock.\n";

		sleep(1);
	}

	return NULL;
}

void threadSynByCondition()
{
	pthread_t tid1, tid2, tid3;
	pthread_create(&tid1, NULL, producer, NULL);
	int id1 = 1, id2 = 2;
	pthread_create(&tid2, NULL, consumer, &id1);
	pthread_create(&tid3, NULL, consumer, &id2);
	pthread_join(tid1, NULL);
	pthread_join(tid2, NULL);
	pthread_join(tid3, NULL);
}

int knapsack(int *W, int *V, int *res, int n, int C)
{
	int value = 0;
	int **f = new int*[n];
	for(int i = 0; i < n; i++)
	{
		f[i] = new int[C+1];
	}

	for(int i = 0; i < n; i++)
		for(int j = 0; j <= C;j++)
			f[i][j] = 0;

	for(int i = 0; i < n; i++)
	{
		f[i][0] = 0;
	}
	for(int i = 1; i <= C; i++)
	{
		f[0][i] = (i < W[0])?0:V[0];
	}
	for(int i = 1; i < n; i++)
	{
		for(int y = 1; y <= C; y++)
		{
			if(y >= W[i])
			{
				f[i][y] = (f[i-1][y] > (f[i-1][y-W[i]] + V[i]))?f[i-1][y]:(f[i-1][y-W[i]] + V[i]);
			} else {
				f[i][y] = f[i-1][y];
			}
		}
	}

	for(int i = 0; i < n; i++)
	{
		for(int j = 0; j < C+1;j++)
			cout << f[i][j] << " ";
		cout << endl;
	}

	value = f[n-1][C];
	int j = n-1;
	int y = C;
	while(j)
	{
		if(f[j][y] == (f[j-1][y-W[j]] + V[j]))
		{
			res[j] = 1;
			y = y - W[j];
		}
		j--;
	}
	if(f[0][y])
	{
		res[0] = 1;
	}

	for(int i = 0; i < n;i++)
	{
		delete f[i];
		f[i] = 0;
	}
	delete [] f;
	f = 0;
	return value;
}

int knapsack(int wBuf[], int vBuf[], int n, int C)
{

	return 0;
}

void knapsackTest1()
{
	int n;//物品种数
	int C;//背包容量
	while(cin >> n >> C)
	{
		int *W = new int[n];//存放所有种类物品的重量
		int *V = new int[n];//存放所有种类物品的价值
		int *res = new int[n];
		for(int i = 0; i < n; i++)
		{
			res[i] = 0;
		}
		int w = 0, v = 0, i = 0;
		while(i < n)
		{
			cin >> w >> v;
			W[i] = w;
			V[i] = v;
			i++;
		}
		int value = knapsack(W, V, res, n, C);
		cout << value << endl;
		for(int i = 0; i < n; i++)
			cout << res[i] << " ";
		cout << endl;
		delete W;
		delete V;
		delete res;
	}
}

void knapsackTest2()
{
	const int n = 5;//物品种数
	int C = 10;//背包容量
	int wBuf[n] = {2, 3, 1, 5, 4};//存放所有种类物品的重量
	int vBuf[n] = {1, 2, 3, 4, 5};//存放所有种类物品的价值
	int res[n] = {0};
	int value = knapsack(wBuf, vBuf, res, n, C);
	cout <<"value = "<< value << endl;
	for(int i = 0; i < n; i++)
		cout << res[i] << " ";
	cout << endl;
}
