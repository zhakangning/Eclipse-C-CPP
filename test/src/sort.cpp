#include "define.h"

inline void printArray(int *a,int len)
{
	int i;
	for(i=0;i<len;i++){
		printf("%d ",a[i]);
	}
	printf("\n");
}

//快速排序
void quickSort(int *a,int start,int end)
{
	printArray(a, end-start+1);
	if(start >= end){
		return ;
	}

	int low = start;
	int high = end;
	int key = a[low];

	while(low < high){
		while(low < high && a[high] > key){
			high--;
		}
		a[low] = a[high];
		cout<<"low = "<<low<<" high = "<<high<<endl;
		printArray(a, 5);
		while(low < high && a[low] < key){
			low++;
		}
		a[high] = a[low];
		cout<<"low = "<<low<<" high = "<<high<<endl;
		printArray(a, 5);
	}
	a[low] = key;
	printArray(a, 5);
	quickSort(a, start, low-1);
	quickSort(a, low+1, end);
}

inline void swap(int &n1, int &n2)
{
	int temp = n1;
	n1 = n2;
	n2 = temp;
}

void quickSort(int *a, int len)
{
	printArray(a, len);
	if(len < 2){//只有一个数的情况就不需要排序了
		return ;
	}
	int iLeft = 0;
	int iRight = len - 1;
	int keyVal = a[0];//取第一个值作为key值
	while(iLeft < iRight){
		while(iLeft < iRight && keyVal < a[iRight]){//从后往前比较，比key值大（等于）的值就和前面的值交换位置
			iRight--;
		}
		swap(a[iLeft], a[iRight]);
		cout<<"iLeft = "<<iLeft<<" iRight = "<<iRight<<endl;
		printArray(a, len);
		while(iLeft < iRight && keyVal > a[iLeft]){//从前往后比较，比key值小（等于）的值就和后面的值交换位置
			iLeft++;
		}
		swap(a[iLeft], a[iRight]);
		//重复上面的比较方式，直到iLeft等于iRight，完成一轮排序
		cout<<"iLeft = "<<iLeft<<" iRight = "<<iRight<<endl;
		printArray(a, len);
	}
	//完成一轮排序之后，key值左边的值都比它小，右边的值都比他大
	quickSort(a, iLeft);//对key左边的部分进行同样的方式排序
	quickSort(a + iLeft + 1, len - iLeft -1);//对key右边的值进行同样的方式排序
}

//5, 3, 2, 1, 0, 4
void insertSort(int *a, int len)
{//直接插入排序
	int i,j,key;
	for(i=1;i<len;i++){
		key=a[i];
		j=i-1;
		while(key < a[j] && j >= 0){
			a[j+1]=a[j];//用key去和它前面的值比较，如果key比较小，就把大值往后移一个位置
			j--;//下标往前移动，看还有没有比key大的值
		}
		//key的前面没有比它大的值了就退出循环，把key值放到第一位或者比它小的值前面
		a[j+1]=key;
	}
}

void selectSort(int *a,int len)
{//直接选择排序
	int i,j,index;
	for(i=0;i<len;i++){
		index=i;
		//找到最小元素的下标
		for(j=i+1;j<len;j++){
			if(a[j]<a[index])
				index=j;
		}
		if(index!=i){//交换位置
			a[index]=a[index]+a[i];
			a[i]=a[index]-a[i];
			a[index]=a[index]-a[i];
		}
	}
}

/*
 * 用数组来存放二叉树，
 * 只要知道任意子节点的下标，
 * 就能推算出其父节点的下标.
 * 通过不断地比较父节点和子节点的大小，
 * 然后交换位置，可以最终把最大（最小）的值放到根节点，
 * 然后把根节点的值和最后一个节点值交换，就可以取出最大值（最小值），
 * 接着再对前面的数运用相同的算法，即可完成堆排序
 */
void heapSort(int *a, int len)
{
	int i, j, root_index;
	for (i = len - 1; i > 0; i--) {
		//从最后一个元素开始向堆的上层查找
		for (j = i; j > 0; j--) {
			if (j % 2 == 0) {
				//右子节点的根节点
				root_index = (j - 2) / 2;
			} else {
				//左子节点的根节点
				root_index = (j - 1) / 2;
			}
			//将大值向前移动
			if (a[root_index] < a[j]) {
				swap(a[root_index], a[j]);
			}
		}
		//将最大值向后移动
		swap(a[j], a[i]);
		printArray(a, len);
	}
	for (i = 0; i < len; i++) {
		printf("%d ", a[i]);
	}
	printf("\n");
}

//内部使用递归
void MergeSort(int sourceArr[], int len)
{
	if(len < 2) return ;
	int mid = len/2;
	MergeSort(sourceArr, mid);//保证前面的集合有序
	MergeSort(sourceArr + mid, mid + len%2);//保证后面的集合有序

	//合并两个有序的集合
	int i = 0, j = mid, k = 0;
	int tempArr[len];
	while(i != mid && j != len)
	{
		if(sourceArr[i] > sourceArr[j])//把小的数字放到临时空间
			tempArr[k++] = sourceArr[j++];
		else
			tempArr[k++] = sourceArr[i++];
	}
	while(i != mid)//把前面集合剩余的数字放入临时空间
		tempArr[k++] = sourceArr[i++];
	while(j != len)//把后面集合剩余的数字放入临时空间
		tempArr[k++] = sourceArr[j++];
	for(i = 0; i < len; i++)//把临时空间中的值放回原来的地方（有序的）
		sourceArr[i] = tempArr[i];
}
