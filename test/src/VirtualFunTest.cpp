/*
 * VirtualFunTest.cpp
 *
 *  Created on: 2018年2月28日
 *      Author: root
 */

#include "define.h"


double Circle::calcArea()
{
	return 3.14*m_dR*m_dR;
}

double Rect::calcArea()
{
	return m_dWidth*m_dHeight;
}

void virtualFunTest()
{
	Shape *shape1=new Circle(4.0);
	Shape *shape2=new Rect(3.0,5.0);
	cout<<shape1->calcArea()<<endl;
	cout<<shape2->calcArea()<<endl;
}
