/*
 * define.h
 *
 *  Created on: 2017年8月8日
 *      Author: zkn
 */

#ifndef DEFINE_H_
#define DEFINE_H_

#include <string>
#include <string.h>
#include <strings.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
//#include <signal.h>
#include <dlfcn.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <signal.h>
#include <vector>
#include <bitset>
#include <deque>
#include <list>
#include <queue>
#include <stack>
#include <utility>
#include <map>
#include <algorithm>
#include <event.h>
#include <hiredis/hiredis.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "threadpool.h"

//#include <pcre.h>

using namespace std;

//#define DEBUG

typedef struct _CHAR2DECIMAL
{
	char c;
	unsigned char bValue;
} CHAR2DECIMAL, *PCHAR2DECIMAL;

string hextostr(string value);
string strtohex(string value);
string strtohex(char value[], int size);
string inttohex(int value, int leng);
int hextoint(string value);
int strtoint(string svalue);
string inttostr(int ivalue);
string floattostr(float ivalue);
unsigned char getcharvalue(char cAscii);

unsigned short GetCRC16(unsigned char *puchMsg, unsigned short usDataLen);
string getchksum(string data);
string get_lenchecksum(string data);
void fileCopy(string filePath);


void printArray(int *a,int len);
//快速排序
void quickSort(int *a,int start,int end);
void quickSort(int *a, int len);
void insertSort(int *a, int len);
void selectSort(int *a,int len);
void heapSort(int *a, int len);
void MergeSort(int sourceArr[], int len);

//select使用例子
void selectTest();
void selectTest2();
//epoll使用
void epollTest();

//libevent使用
int openServer(const unsigned short &port);
void releaseSockEvent(struct sockEvent* ev);
void handleWrite(int sock, short event, void* arg);
void handldRead(int sock, short event, void* arg);
void handleAccept(int sock, short event, void* arg);
void libeventTest();

//redis使用
void redisTest();

class Shape//形状类
{
public:
	virtual ~Shape(){}
	virtual double calcArea()
	{
		cout<<"calcArea"<<endl;
		return 0;
	}

};

//虚函数使用
class Circle:public Shape      //公有继承自形状类的圆形类
{
public:
	Circle(double r){
		m_dR = r;
	}
	virtual double calcArea();
private:
	double m_dR;
};

class Rect:public Shape       //公有继承自形状类的矩形类
{
public:
	Rect(double width,double height){
		m_dWidth = width;
		m_dHeight = height;
	}
	virtual double calcArea();
private:
	double m_dWidth;
	double m_dHeight;
};

void virtualFunTest();


//信号处理
void catch_signal(int sign);
void signalTest();
void alarmSignalTest();

//二叉树
typedef struct tree{
	char data;
	struct tree *left,*right;
}BTREE;
BTREE *createBTree(int *a);

//递归方式前序遍历
void preOrderVisit(BTREE *rootNode);

//前序遍历用数组结构存储的完全二叉树
void preOrderVisit(int *a, int rootIndex, int len);

//使用栈来前序遍历二叉树
void preOrderVisitByStack(BTREE *rootNode);

//使用栈来遍历用数组存储的完全二叉树
void preOrderVisitByStack(int *a, int len);

void inOrderVisit(BTREE *rootNode);
void postOrderVisit(BTREE *rootNode);
void levelVisit(BTREE *rootNode);
int getDepth(BTREE *rootNode);
//通过二叉树的前序遍历和中序遍历生成二叉树，必须要知道两个序列才能唯一确定一个二叉树
//参数：先序序列字符串、中序序列字符串、节点数
BTREE *createTreeByPreOrderAndInOrder(char *pre, char *in, int n);
int getLevel(BTREE *root ,char data);
//初始化一个简单的完全二叉树
BTREE * initBinaryTree();
//初始化一个简单的完全二叉树，用数组来存储
void initBinaryTree(int *a, int n);

//八皇后
void solveEightQueen();
//检查当前位置是否可以落子，可以落子返回true，不能落子返回false
bool check(int chessboard[][8], int row, int col);
//在棋盘中标记不能落子的格子，便于后面落子的判断
//标记为2表示没有落子但是不能落子，1表示已经落子同样也不能落子，0表示可以落子
void markInChessboard(int chessboard[][8], int row, int col);
void printChessboard(int chessboard[][8]);

/*指定位置落子
 * chessboard：棋盘
 * row：指定落子的行
 * lastCol：前一行的落子的列
 */
void setChess(int chessboard[][8], int row, int lastCol);

//背包问题
int knapsack(int *W, int *V, int *res, int n, int C);
int knapsack(int wBuf[], int vBuf[], int n, int C);
void knapsackTest1();
void knapsackTest2();

void udpTest();

//友元类和友元函数的测试
void friendTest();
//声明教师类
class Techer ;
//学生类
class Student
{
private:
	string name ;
	int age ;
	char sex ;
	int score ;
public :
	Student(string name , int age , char sex , int score);
	void stu_print(Techer &tech);
};
//教师类
class Techer
{
private:
	string name ;
	int age ;
	char sex ;
	int score ;
public :
	Techer(string name , int age , char sex , int score);
	//声明一个友元类
	friend class Student ;
	//声明一个友元函数
	friend void printInfo(Techer &t);
};

//线程池测试
void threadPoolTest();
void* mytask(void *arg);

//条件变量线程同步测试
static deque <int> productQueue;
static pthread_mutex_t lock = PTHREAD_MUTEX_INITIALIZER;
static pthread_cond_t cond4c = PTHREAD_COND_INITIALIZER;//消费者等待的条件
static pthread_cond_t cond4p = PTHREAD_COND_INITIALIZER;//生产者等待的条件
void threadSynByCondition();

#endif /* DEFINE_H_ */

